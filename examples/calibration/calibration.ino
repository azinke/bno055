/** Calibbration of the BNO055 sensor
    @author: AMOUSSOU Z. Kenneth
    @date: 25-02-2019
    @version:
*/
/*
Calibration procedure
----------------------
[1] Accelerometer calibration
    Rotate the sensor around one axis (x, y or z) in six different
    position. Make slow rotation till the accelerometer calibration 
    status reach "3"
[2] Gyroscope calibration
    Just keep the sensor flat and still for a while. It is actually easy and
    straight forward
[3] Magnetometer
    Perform random mouvements with the sensor (like turning around in the air) 
    till the magnetometer calibration status reached "3".

Status:
--------
    0: sensor not calibrated
    1: poorly calirated
    2: calibrated (but not well)
    3: fully calibrated
*/
#include <BNO055.h>

BNO055 imu;

void setup(){
    Serial.begin(115200);
    imu.begin();    
}

void loop(){
    Serial.print("System calibration status: ");
    Serial.println(imu.getSysCalStatus());
    Serial.print("Gyroscope calibration status: ");
    Serial.println(imu.getGyroCalStatus());
    Serial.print("Accelerometer calibration status: ");
    Serial.println(imu.getAccelCalStatus());
    Serial.print("Magnetometer calibration status: ");
    Serial.println(imu.getMagCalStatus());
    Serial.println();
    delay(250);
}
