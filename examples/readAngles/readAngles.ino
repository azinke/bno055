/*

*/
#include <BNO055.h>

BNO055 imu;

EUL angles;
int8_t temperature = 0;

void setup(){
    Serial.begin(9600);
    delay(100);
    while(!imu.isConnected()){
        Serial.println("BNO055 not found!");
    }
    Serial.println("BNO055 connected!");
    imu.begin(); 
}

void loop(){
    angles = imu.getAngles();

    Serial.print("Roll, Pitch, Yaw: ");
    Serial.print(angles.roll);
    Serial.print(", ");
    Serial.print(angles.pitch);
    Serial.print(", ");
    Serial.println(angles.yaw);
    Serial.println();
    delay(1000);
}
