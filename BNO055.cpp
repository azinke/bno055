/** BNO055 library
 * @author: AMOUSSOU Z. Kenneth
 * @date: 15-01-2019
 * @version:
 */

#include <BNO055.h>

/*
 *  @function: constructor
 *  @summary:
 *  @param: none
 *  @return: none
 */
BNO055::BNO055(){
    // constructor
}

/*
 *  @function: _read8
 *  @summary: read a byte from the two wire interface
 *  @param:
 *      _addr: device address
 *      _register: address of the register to be read
 *  @return:
 *      uint8_t: value of the selected register
 */
uint8_t BNO055::_read8(uint8_t _addr, uint8_t _register){
    Wire.beginTransmission(_addr);
    Wire.write(_register);
    Wire.endTransmission(false);
#ifdef __STM32F1__
    Wire.requestFrom(_addr, 1);
#else
    Wire.requestFrom(_addr, 1, true);
#endif
    return Wire.read();
}

/*
 *  @function: _read16
 *  @summary: read a word from the two wire interface
 *  @param:
 *      _addr: device address
 *      _register: starting address of the register to be read
 *  @return:
 *      int16_t: value of the selected register
 *                  (MSB << 8) | LSB
 */
int16_t BNO055::_read16(uint8_t _addr, uint8_t _register){
    Wire.beginTransmission(_addr);
    Wire.write(_register);
    Wire.endTransmission(false);
#ifdef __STM32F1__
    Wire.requestFrom(_addr, 2);
#else
    Wire.requestFrom(_addr, 2, true);
#endif
    int16_t buffer = Wire.read();
    buffer |= Wire.read() << 8;
    return buffer;
}

/*
 *  @function: _write8
 *  @summary: write a byte on the two wire interface
 *  @param:
 *      _addr: device address
 *      _register: address of the register to be written
 *      _data: data to write
 *  @return: none
 */
void BNO055::_write8(uint8_t _addr, uint8_t _register, uint8_t _data){
    Wire.beginTransmission(_addr);
    Wire.write(_register);
    Wire.write(_data);
    Wire.endTransmission(true);
}

/*
 *  @function: begin
 *  @summary: initialize the library
 *  @param: none
 *  @return: none
 */
void BNO055::begin(){
    Wire.begin();
    _config = _read8(BNO_ADDR, BNO_UNIT_SEL);
    setMode(BNO_NDOF);  // keep this always at the end of this function
}

/*
 *  @function: setMode
 *  @summary: set operation mode
 *  @param: 
 *      mode
 *  @return: none
 */
void BNO055::setMode(uint8_t mode){
    if(mode > 12) return;
    _write8(BNO_ADDR, BNO_OPR_MODE, mode);
    delay(100);
}

/*
 *  @function: getMode
 *  @summary: get the operation mode of the sensor
 *  @param: none 
 *  @return: mode
 *      predefined value form 0x00 to 0x0C that describe the current
 *      checkout "Operation modes" in header file
 */
uint8_t BNO055::getMode(){
    return _read8(BNO_ADDR, BNO_OPR_MODE);
}

/*
 *  @function: setPage
 *  @summary: change register page
 *  @para: 
 *      page:
 *          [0]: select page 0
 *          [1]: select page 1
 *  @return: status
 */
bool BNO055::setPage(uint8_t page){
    if(page > 1) return false;
    uint8_t mode = _read8(BNO_ADDR, BNO_OPR_MODE);
    setMode(BNO_CONFIG_MODE);
    _write8(BNO_ADDR, BNO_PAGE_ID, page);
    setMode(BNO_CONFIG_MODE);
    return true;
}

/*
 *  @function: isConnected
 *  @summary: wiring checking
 *  @param: none
 *  @return: none
 */
bool BNO055::isConnected(){
#if BNO_DEBUG == 0x01
    Serial.print("ID: 0x");
    Serial.println(_read8(BNO_ADDR, BNO_CHIP_ID), HEX);
#endif
    if(_read8(BNO_ADDR, BNO_CHIP_ID) == BNO_ID_DEF) return true;
    else return false;
}

/*
 *  @function: setOrientation
 *  @summary:set the sensor orientation
 *  @param:
 *      orientation:
 *          [windows]: in this mode, the sensor pitch angle increase in
 *                     clockwise rotation
 *          [android]: in this mode, the sensor pitch angle descreases in
 *                     clockwise rotation
 *  @return: none
 */
void BNO055::setOrientation(bool orientation){
    uint8_t buffer = _read8(BNO_ADDR, BNO_UNIT_SEL);
    if(!orientation) buffer &=  0x7F;
    else buffer |= (1 << 7);
    _write8(BNO_ADDR, BNO_UNIT_SEL, buffer);
}

/*
 *  @function: setUnits
 *  @summary: set units for different data
 *  @param:
 *      accel:
 *          [0]: m/s²
 *          [1]: mg
 *      gyro:
 *          [0]: °/s
 *          [1]: rad/s
 *      temp:
 *          [0]: °C
 *          [1]: °F
 *      eul:
 *          [0]: °
 *          [1]: rad
 *  @return: none
 */
void BNO055::setUnits(bool accel, bool gyro, bool temp, bool eul){
    uint8_t buffer = _read8(BNO_ADDR, BNO_UNIT_SEL);
    buffer &= 0xC0;
    buffer |= (uint8_t)accel | ((uint8_t)gyro << 1);
    buffer |= ((uint8_t)eul << 2) | ((uint8_t)temp << 4);
    // read current mode
    uint8_t mode = getMode();
    // switch to configuration mode
    setMode(BNO_CONFIG_MODE);
    _write8(BNO_ADDR, BNO_UNIT_SEL, buffer);
    _config = buffer;
    // switch back to previews mode
    setMode(mode);
}

/*
 *  @function: getStatus
 *  @summary: read the system status
 *  @param: none
 *  @return:
 *      uint8_t: system status
 */
uint8_t BNO055::getStatus(){
    uint8_t buffer = _read8(BNO_ADDR, BNO_SYS_STATUS);
#if BNO_DEBUG == 0x01
    Serial.print("System status code: ");
    Serial.println(buffer);

    switch(buffer){
        case 0:{
            Serial.println("\t > System idle!");
            break;
        }
        case 1:{
            Serial.println("\t > System error!");
            break;
        }
        case 2:{
            Serial.println("\t > Initializing perpherals!");
            break;
        }
        case 3:{
            Serial.println("\t > System Initialisation!");
            break;
        }
        case 4:{
            Serial.println("\t > Executing self test!");
            break;
        }
        case 5:{
            Serial.println("\t > Sensor fusion algorithm running!");
            break;
        }
        case 6:{
            Serial.println("\t > System running without fusion algorithm!");
            break;
        }
    }
#endif
    return buffer;
}

/*
 *  @function: getError
 *  @summary: read the system error code
 *  @param: none
 *  @return:
 *      uint8_t: system error code
 */
uint8_t BNO055::getError(){
    uint8_t error = _read8(BNO_ADDR, BNO_SYS_ERR);
#if BNO_DEBUG == 0x01
    Serial.print("System error code: ");
    Serial.println(error);

    switch(error){
        case 0:{
            Serial.println("\t > No error!");
            break;
        }
        case 1:{
            Serial.println("\t > Peripheral initialization error");
            break;
        }
        case 2:{
            Serial.println("\t > System initialisation error.");
            break;
        }
        case 3:{
            Serial.println("\t > Self test result failed.");
            break;
        }
        case 4:{
            Serial.println("\t > Register map out of range.");
            break;
        }
        case 5:{
            Serial.println("\t > Register map address out of range.");
            break;
        }
        case 6:{
            Serial.println("\t > Register map write error.");
            break;
        }
        case 7:{
            Serial.println("\t > BNO low power mode not available for \
                    selected peration mode");
            break;
        }
        case 8:{
            Serial.println("\t > Accelerometer power mode not available.");
            break;
        }
        case 9:{
            Serial.println("\t > Fusion algorithm configuration error.");
            break;
        }
        case 10:{
            Serial.println("\t > Sensor configuration error.");
            break;
        }
    }
#endif
    return error;
}

/*
 *  @function: getAngles
 *  @summary: read euler angles
 *  @param: none
 *  @return:
 *      EULER: euler angles data structure
 */
EUL BNO055::getAngles(){
    EUL angles;
    float scale = 16.0;
    if(_config & 0x04) scale = 900.0;
    angles.roll = (float)_read16(BNO_ADDR, BNO_EUL_DATA_ROLL_LSB) / scale;
    angles.pitch = (float)_read16(BNO_ADDR, BNO_EUL_DATA_PITCH_LSB) / scale;
    angles.yaw = (float)_read16(BNO_ADDR, BNO_EUL_DATA_YAW_LSB) / scale;
    return angles;
}

/*
 *  @function: getGyro
 *  @summary: read gyroscope data
 *  @param: none
 *  @return:
 *      XYZ: x, y, z axis data structure
 *      mask: 0x02
 */
XYZ BNO055::getGyro(){
    XYZ data;
    float scale = 16.0;
    if(_config & 0x02) scale = 900.0;
    data.x = (float)_read16(BNO_ADDR, BNO_GYR_DATA_X_LSB) / scale;
    data.y = (float)_read16(BNO_ADDR, BNO_GYR_DATA_Y_LSB) / scale;
    data.z = (float)_read16(BNO_ADDR, BNO_GYR_DATA_Z_LSB) / scale;
    return data;
}

/*
 *  @function: getAccel
 *  @summary: read accelerometer data
 *  @param: none
 *  @return:
 *      XYZ: x, y, z axis data structure
 *      mask: 0x01
 */
XYZ BNO055::getAccel(){
    XYZ data;
    float scale = 100.0;
    if(_config & 0x01) scale = 1;
    data.x = (float)_read16(BNO_ADDR, BNO_GYR_DATA_X_LSB) / scale;
    data.y = (float)_read16(BNO_ADDR, BNO_GYR_DATA_Y_LSB) / scale;
    data.z = (float)_read16(BNO_ADDR, BNO_GYR_DATA_Z_LSB) / scale;
    return data;
}

/*
 *  @function: getMag
 *  @summary: read magnetometer data
 *  @param: none
 *  @return:
 *      XYZ: x, y, z axis data structure
 */
XYZ BNO055::getMag(){
    XYZ data;
    data.x = _read16(BNO_ADDR, BNO_MAG_DATA_X_LSB);
    data.y = _read16(BNO_ADDR, BNO_MAG_DATA_Y_LSB);
    data.z = _read16(BNO_ADDR, BNO_MAG_DATA_Z_LSB);
    return data;
}

/*
 *  @function: getQuat
 *  @summary: read quaternion out of the sensor
 *  @param: none
 *  @return:
 *      QUATERNION: w, x, y, z data (a quaternion)
 *  @note: only available in fusion mode
 */
QUATERNION BNO055::getQuat(){
    QUATERNION data;
    data.w = (float)_read16(BNO_ADDR, BNO_QUA_DATA_W_LSB)/16384.0;
    data.x = (float)_read16(BNO_ADDR, BNO_QUA_DATA_X_LSB)/16384.0;
    data.y = (float)_read16(BNO_ADDR, BNO_QUA_DATA_Y_LSB)/16384.0;
    data.z = (float)_read16(BNO_ADDR, BNO_QUA_DATA_Z_LSB)/16384.0;
    return data;
}

/*
 *  @function: getLinAccel
 *  @summary: read linear acceleration data
 *  @param: none
 *  @return:
 *      XYZ: x, y, z axis data structure
 *  @note: only available in fusion mode
 *  mask: 0x01
 */
XYZ BNO055::getLinAccel(){
    XYZ data;
    float scale = 100.0;
    if(_config & 0x01) scale = 1;
    data.x = _read16(BNO_ADDR, BNO_LIA_DATA_X_LSB) / scale;
    data.y = _read16(BNO_ADDR, BNO_LIA_DATA_Y_LSB) / scale;
    data.z = _read16(BNO_ADDR, BNO_LIA_DATA_Z_LSB) / scale;
    return data;
}

/*
 *  @function: getGravity
 *  @summary: read gravity vector data
 *  @param: none
 *  @return:
 *      XYZ: x, y, z axis data structure
 *  @note: only available in fusion mode
 *  mask: 0x01
 */
XYZ BNO055::getGravity(){
    XYZ data;
    float scale = 100.0;
    if(_config & 0x01) scale = 1;
    data.x = _read16(BNO_ADDR, BNO_GRV_DATA_X_LSB) / scale;
    data.y = _read16(BNO_ADDR, BNO_GRV_DATA_Y_LSB) / scale;
    data.z = _read16(BNO_ADDR, BNO_GRV_DATA_Z_LSB) / scale;
    return data;
}

/*
 *  @function: getTemp
 *  @summary: read temperature out of the sensor
 *  @param: none
 *  @return:
 *      int8_t: temperature data
 */
int8_t BNO055::getTemp(){
    return (int8_t) _read8(BNO_ADDR, BNO_TEMP);
}

/*
 *  @function: setTempSource
 *  @summary: set temperature source of the sensor
 *  @param: 
 *      source:
 *          [0]: accelerometer
 *          [1]: gyroscope
 *  @return: none
 */
void BNO055::setTempSource(bool source){
    // read current mode
    uint8_t mode = getMode();
    setMode(BNO_CONFIG_MODE);
    _write8(BNO_ADDR, BNO_TEMP_SOURCE, source);
    // switch back to previews mode
    setMode(mode);
}

/*
 *  @function: getSysCalStatus
 *  @summary: read the sensor (system) calibration status
 *  @param: none 
 *  @return:
 *      0: not calibrated
 *      1: poorly calibrated
 *      2: calibrated
 *      3: fully calibrated
 */
uint8_t BNO055::getSysCalStatus(){
    uint8_t buffer = _read8(BNO_ADDR, BNO_CALIB_STAT);
    buffer >>= 6;
    return buffer;
}

/*
 *  @function: getGyroCalStatus
 *  @summary: read the gyroscope calibration status
 *  @param: none 
 *  @return:
 *      0: not calibrated
 *      1: poorly calibrated
 *      2: calibrated
 *      3: fully calibrated
 */
uint8_t BNO055::getGyroCalStatus(){
    uint8_t buffer = _read8(BNO_ADDR, BNO_CALIB_STAT);
    buffer >>= 4;
    buffer &= 0x03;
    return buffer;
}

/*
 *  @function: getAccelCalStatus
 *  @summary: read the accelerometer calibration status
 *  @param: none 
 *  @return:
 *      0: not calibrated
 *      1: poorly calibrated
 *      2: calibrated
 *      3: fully calibrated
 */
uint8_t BNO055::getAccelCalStatus(){
    uint8_t buffer = _read8(BNO_ADDR, BNO_CALIB_STAT);
    buffer >>= 2;
    buffer &= 0x03;
    return buffer;
}

/*
 *  @function: getMagCalStatus
 *  @summary: read the magnetometer calibration status
 *  @param: none 
 *  @return:
 *      0: not calibrated
 *      1: poorly calibrated
 *      2: calibrated
 *      3: fully calibrated
 */
uint8_t BNO055::getMagCalStatus(){
    uint8_t buffer = _read8(BNO_ADDR, BNO_CALIB_STAT);
    buffer &= 0x03;
    return buffer;
}
