/** BNO055 library
 * @author: AMOUSSOU Z. Kenneth
 * @date: 15-01-2019
 * @version:
 */

#ifndef H_BNO055
#define H_BNO055

#if ARDUINO < 100
    #include <WProgram.h>
#else
    #include <Arduino.h>
#endif
#include <Wire.h>

/*
 * Registers definitions
 */

// 
// Register Page 0
//

#define BNO_CHIP_ID                     0x00
#define BNO_ACC_ID                      0x01
#define BNO_MAG_ID                      0x02
#define BNO_GYR_ID                      0x03
#define BNO_SW_REV_ID_LSB               0x04
#define BNO_SW_REV_ID_MSB               0x05
#define BNO_BL_REV_ID                   0x06
#define BNO_PAGE ID                     0x07

#define BNO_ACC_DATA_X_LSB              0x08
#define BNO_ACC_DATA_X_MSB              0x09
#define BNO_ACC_DATA_Y_LSB              0x0A
#define BNO_ACC_DATA_Y_MSB              0x0B
#define BNO_ACC_DATA_Z_LSB              0x0C
#define BNO_ACC_DATA_Z_MSB              0x0D

#define BNO_MAG_DATA_X_LSB              0x0E
#define BNO_MAG_DATA_X_MSB              0x0F
#define BNO_MAG_DATA_Y_LSB              0x10
#define BNO_MAG_DATA_Y_MSB              0x11
#define BNO_MAG_DATA_Z_LSB              0x12
#define BNO_MAG_DATA_Z_MSB              0x13

#define BNO_GYR_DATA_X_LSB              0x14
#define BNO_GYR_DATA_X_MSB              0x15
#define BNO_GYR_DATA_Y_LSB              0x16
#define BNO_GYR_DATA_Y_MSB              0x17
#define BNO_GYR_DATA_Z_LSB              0x18
#define BNO_GYR_DATA_Z_MSB              0x19

#define BNO_EUL_DATA_YAW_LSB            0x1A
#define BNO_EUL_DATA_YAW_MSB            0x1B
#define BNO_EUL_DATA_ROLL_LSB           0x1C
#define BNO_EUL_DATA_ROLL_MSB           0x1D
#define BNO_EUL_DATA_PITCH_LSB          0x1E
#define BNO_EUL_DATA_PITCH_MSB          0x1F

#define BNO_QUA_DATA_W_LSB              0x20
#define BNO_QUA_DATA_W_MSB              0x21
#define BNO_QUA_DATA_X_LSB              0x22
#define BNO_QUA_DATA_X_MSB              0x23
#define BNO_QUA_DATA_Y_LSB              0x24
#define BNO_QUA_DATA_Y_MSB              0x25
#define BNO_QUA_DATA_Z_LSB              0x26
#define BNO_QUA_DATA_Z_MSB              0x27

#define BNO_LIA_DATA_X_LSB              0x28
#define BNO_LIA_DATA_X_MSB              0x29
#define BNO_LIA_DATA_Y_LSB              0x2A
#define BNO_LIA_DATA_Y_MSB              0x2B
#define BNO_LIA_DATA_Z_LSB              0x2C
#define BNO_LIA_DATA_Z_MSB              0x2D

#define BNO_GRV_DATA_X_LSB              0x2E
#define BNO_GRV_DATA_X_MSB              0x2F
#define BNO_GRV_DATA_Y_LSB              0x30
#define BNO_GRV_DATA_Y_MSB              0x31
#define BNO_GRV_DATA_Z_LSB              0x32
#define BNO_GRV_DATA_Z_MSB              0x33

#define BNO_TEMP                        0x34
#define BNO_CALIB_STAT                  0x35
#define BNO_ST_RESULT                   0x36
#define BNO_INT_STA                     0x37
#define BNO_SYS_CLK_STATUS              0x38
#define BNO_SYS_STATUS                  0x39
#define BNO_SYS_ERR                     0x3A
#define BNO_UNIT_SEL                    0x3B

#define BNO_OPR_MODE                    0x3D
#define BNO_PWR_MODE                    0x3E
#define BNO_SYS_TRIGGER                 0x3F
#define BNO_TEMP_SOURCE                 0x40
#define BNO_AXIS_MAP_CONFIG             0x41
#define BNO_AXIS_MAP_SIGN               0x42

#define BNO_ACC_OFFSET_X_LSB            0x55
#define BNO_ACC_OFFSET_X_MSB            0x56
#define BNO_ACC_OFFSET_Y_LSB            0x57
#define BNO_ACC_OFFSET_Y_MSB            0x58
#define BNO_ACC_OFFSET_Z_LSB            0x59
#define BNO_ACC_OFFSET_Z_MSB            0x5A

#define BNO_MAG_OFFSET_X_LSB            0x5B
#define BNO_MAG_OFFSET_X_MSB            0x5C
#define BNO_MAG_OFFSET_Y_LSB            0x5D
#define BNO_MAG_OFFSET_Y_MSB            0x5E
#define BNO_MAG_OFFSET_Z_LSB            0x5F
#define BNO_MAG_OFFSET_Z_MSB            0x60

#define BNO_GYR_OFFSET_X_LSB            0x61
#define BNO_GYR_OFFSET_X_MSB            0x62
#define BNO_GYR_OFFSET_Y_LSB            0x63
#define BNO_GYR_OFFSET_Y_MSB            0x64
#define BNO_GYR_OFFSET_Z_LSB            0x65
#define BNO_GYR_OFFSET_Z_MSB            0x66

#define BNO_ACC_RADIUS_LSB              0x67
#define BNO_ACC_RADIUS_MSB              0x68

#define BNO_MAG_RADIUS_LSB              0x69
#define BNO_MAG_RADIUS_MSB              0x6A

// 
// Register Page 1
//

#define BNO_PAGE_ID                     0x07
#define BNO_ACC_CONFIG                  0x08
#define BNO_MAG_CONFIG                  0x09
#define BNO_GYR_CONFIG_0                0x0A
#define BNO_GYR_CONFIG_1                0x0B

#define BNO_ACC_SLEEP_CONFIG            0x0C
#define BNO_GYR_SLEEP_CONFIG            0x0D

#define BNO_INT_MSK                     0x0F
#define BNO_INT_EN                      0x10

#define BNO_ACC_AM_THRES                0x11
#define BNO_ACC_INT_SETTINGS            0x12
#define BNO_ACC_HG_DURATION             0x13
#define BNO_ACC_HG_THRES                0x14
#define BNO_ACC_NM_THRES                0x15
#define BNO_ACC_NM_SET                  0x16


#define BNO_GYR_INT_SETTING             0x17
#define BNO_GYR_HR_X_SET                0x18
#define BNO_GYR_DUR_X                   0x19
#define BNO_GYR_HR_Y_SET                0x1A
#define BNO_GYR_DUR_Y                   0x1B
#define BNO_GYR_HR_Z_SET                0x1C
#define BNO_GYR_DUR_Z                   0x1D
#define BNO_GYR_AM_THRES                0x1E
#define BNO_GYR_AM_SET                  0x1F

/*
 * Power modes
 */
#define BNO_NORMAL                      0x00
#define BNO_LOW_POWER                   0x01
#define BNO_SUSPEND                     0x02

/*
 * Operation modes
 */
#define BNO_CONFIG_MODE                 0x00
#define BNO_ACC_ONLY                    0x01
#define BNO_MAG_ONLY                    0x02
#define BNO_GYRO_ONLY                   0x03
#define BNO_ACC_MAG                     0x04
#define BNO_ACC_GYRO                    0x05
#define BNO_MAG_GYRO                    0x06
#define BNO_AMG                         0x07
#define BNO_IMU                         0x08
#define BNO_COMPASS                     0x09
#define BNO_M4G                         0x0A
#define BNO_NDOF_FMC_OFF                0x0B
#define BNO_NDOF                        0x0C

/*
 * Accelerometer configurations
 */

// Range configuration
#define BNO_ACC_2G                      0x00
#define BNO_ACC_4G                      0x01
#define BNO_ACC_8G                      0x02
#define BNO_ACC_16G                     0x03

// Bandwidth configuration
#define BNO_ACC_7Hz                     (0x00 << 2)
#define BNO_ACC_15Hz                    (0x01 << 2)
#define BNO_ACC_31Hz                    (0x02 << 2)
#define BNO_ACC_62Hz                    (0x03 << 2)
#define BNO_ACC_125Hz                   (0x04 << 2)
#define BNO_ACC_250Hz                   (0x05 << 2)
#define BNO_ACC_500Hz                   (0x06 << 2)
#define BNO_ACC_1000Hz                  (0x07 << 2)

// Operation mode configuration
#define BNO_ACC_NORMAL                  (0x00 << 5)
#define BNO_ACC_SUSPEND                 (0x01 << 5)
#define BNO_ACC_LP1                     (0x02 << 5)   // low power mode 1
#define BNO_ACC_STANDBY                 (0x03 << 5)
#define BNO_ACC_LP2                     (0x04 << 5)    // low poxer mode 2
#define BNO_ACC_DEEP_SUSPEND            (0x05 << 5)

/*
 * Gyroscope configuration
 */

// Range configuration
#define BNO_GYR_2000dps                 0x00
#define BNO_GYR_1000dps                 0x01
#define BNO_GYR_500dps                  0x02
#define BNO_GYR_250dps                  0x03
#define BNO_GYR_125dps                  0x04

// Bandwidth configuration
#define BNO_GYR_523Hz                   (0x00 << 3)
#define BNO_GYR_230Hz                   (0x01 << 3)
#define BNO_GYR_116Hz                   (0x02 << 3)
#define BNO_GYR_47Hz                    (0x03 << 3)
#define BNO_GYR_23Hz                    (0x04 << 3)
#define BNO_GYR_12Hz                    (0x05 << 3)
#define BNO_GYR_64Hz                    (0x06 << 3)
#define BNO_GYR_32Hz                    (0x07 << 3)

// Operation mode configuration
#define BNO_GYR_NORMAL                  0x00
#define BNO_GYR_FAST_POWER_UP           0x01
#define BNO_GYR_DEEP_SUSPEND            0x02
#define BNO_GYR_SUSPEND                 0x03
#define BNO_GYR_ADVANCED_POWERSAVE      0x04
 
/*
 * Magnetometer configuration
 */

// Bandwidth configuration
#define BNO_MAG_2Hz                     0x00
#define BNO_MAG_6Hz                     0x01
#define BNO_MAG_8Hz                     0x02
#define BNO_MAG_10Hz                    0x03
#define BNO_MAG_15Hz                    0x04
#define BNO_MAG_20Hz                    0x05
#define BNO_MAG_25Hz                    0x06
#define BNO_MAG_30Hz                    0x07

// Operation mode configuration
#define BNO_MAG_LP                      (0x00 << 3)
#define BNO_MAG_REGULAR                 (0x00 << 3)
#define BNO_MAG_ENHANCED_REGULAR        (0x00 << 3)
#define BNO_MAG_HIGH_ACCURACY           (0x00 << 3)

// Power mode configuration
#define BNO_MAG_NORMAL                  (0x00 << 5)
#define BNO_MAG_SLEEP                   (0x01 << 5)
#define BNO_MAG_SUSPEND                 (0x02 << 5)
#define BNO_MAG_FORCE_MODE              (0x03 << 5)

/**
 *  I2C Interface & General purpose definition
 */

// BNO055 I2C address
#define BNO_ADDR                        0x29

// output unit
#define BNO_EUL                         0x00    // Euler angles
#define BNO_QUA                         0x01    // Quaternions

// measurement data units
#define BNO_GYR_DEG                      0x00    //  °/s
#define BNO_GYR_RAD                      0x01    //  rad/s

#define BNO_ACC_MS2                      0x00    //  m/s²
#define BNO_ACC_MG                       0x01    //  mg
 
#define BNO_EUL_DEG                      0x00    //  °
#define BNO_EUL_RAD                      0x01    //  rad

#define BNO_TEMP_DF                      0x00    //  °C
#define BNO_TEMP_DC                      0x01    //  °F

// Chips default ID values
#define BNO_ID_DEF                      0xA0
#define BNO_ACC_ID_DEF                  0xFB
#define BNO_GYR_ID_DEF                  0x0F
#define BNO_MAG_ID_DEF                  0x32

// sensor orientation
#define BNO_WINDOWS                     0x00
#define BNO_ANDROID                     0x01

#define BNO_DEBUG                       0x00

/*
 * Data structure
 */
typedef struct{
    float roll;
    float pitch;
    float yaw;
}EUL;

typedef struct{
    float x;
    float y;
    float z;
}XYZ;

typedef struct{
    float w;
    float x;
    float y;
    float z;
}QUATERNION;

class BNO055{
    public:
        BNO055();
        void begin();
        bool isConnected();
        bool setPage(uint8_t page);
        void setMode(uint8_t mode);
        uint8_t getMode();
        
        // windows or android orientation
        void setOrientation(bool orientation);
        void setUnits(bool accel, bool gyro, bool temp, bool eul);

        uint8_t getStatus();   // read out system status code
        uint8_t getError();    // read out system error code

        EUL getAngles();     // read out Euler angles
        XYZ getGyro();
        XYZ getAccel();
        XYZ getMag();
        QUATERNION getQuat();
        XYZ getLinAccel();
        XYZ getGravity();
        int8_t getTemp();
        void setTempSource(bool source);

        // read sensor calibration status
        // 0: not calibrated
        // 1: fully calibrated
        uint8_t getSysCalStatus();
        uint8_t getAccelCalStatus();
        uint8_t getGyroCalStatus();
        uint8_t getMagCalStatus();


    private:
        uint8_t _read8(uint8_t _addr, uint8_t _register);
        int16_t _read16(uint8_t _addr, uint8_t _register);
        void _write8(uint8_t _addr, uint8_t _register, uint8_t _data);

        uint8_t _config;
};

#endif
